package cz.cvut.fel.hw_04.archive;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import cz.cvut.fel.hw_04.shop.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;



class PurchasesArchiveTest {
    private final PurchasesArchive testPurchase = new PurchasesArchive();
    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private final PrintStream printStream = System.out;

    @Mock
    private ArrayList<Order> order;
    @Mock
    private ItemPurchaseArchiveEntry itemPurchaseArchiveEntry;

    @BeforeEach
    void setUp() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(new StandardItem(0, "smith", 500, "X", 5));
        shoppingCart.addItem(new StandardItem(1, "johnson", 100, "Y", 3));
        shoppingCart.addItem(new DiscountedItem(2, "Chocolate Bar", 10, "Z", 10,  "1.1.2023", "1.1.2024"));
        Order order1 = new Order(shoppingCart, "Vít", "Troppau");
        testPurchase.putOrderToPurchasesArchive(order1);
        System.setOut(new PrintStream(outputStream));
    }
    @AfterEach
    void resStream(){
        System.setOut(printStream);
    }
    @Test
    void testItemPurchaseArchiveEntry(){
        StandardItem standardItem = new StandardItem(1, "johnson", 100, "Y", 3);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(standardItem);

        Assertions.assertEquals(standardItem, itemPurchaseArchiveEntry.getRefItem());}
    @Test
    void printItemPurchaseStatistics() {
        testPurchase.printItemPurchaseStatistics();
        Collection<ItemPurchaseArchiveEntry> itemPurchaseArchiveEntryCollection = testPurchase.getItemPurchaseArchiveEntryValues();
        StringBuilder expectedOut = new StringBuilder();
        expectedOut.append("ITEM PURCHASE STATISTICS:\n");
        for (ItemPurchaseArchiveEntry entry : itemPurchaseArchiveEntryCollection) {
        expectedOut.append(entry.toString()).append("\n");
        }
        Assertions.assertEquals(expectedOut.toString(), outputStream.toString());
    }

    @Test
    void getHowManyTimesHasBeenItemSold() {
        StandardItem standardItem1 = new StandardItem(9, "chuan", 200, "V", 100);
        StandardItem standardItem2 = new StandardItem(1, "johnson", 100, "Y", 3);
        DiscountedItem discountedItem1 = new DiscountedItem(2, "Chocolate Bar", 10, "Z", 10,  "1.1.2023", "1.1.2024");
        int count1 = testPurchase.getHowManyTimesHasBeenItemSold(standardItem1);
        int count2 = testPurchase.getHowManyTimesHasBeenItemSold(standardItem2);
        int count3 = testPurchase.getHowManyTimesHasBeenItemSold(discountedItem1);

        Assertions.assertEquals(0, count1);
        Assertions.assertEquals(1, count2);
        Assertions.assertEquals(1, count3);
    }

    @Test
    void putOrderToPurchasesArchive() {
        StandardItem standardItem = new StandardItem(1, "johnson", 100, "Y", 3);
        Assertions.assertEquals(1, testPurchase.getHowManyTimesHasBeenItemSold(standardItem));
    }

}