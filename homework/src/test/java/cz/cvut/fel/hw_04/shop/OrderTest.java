package cz.cvut.fel.hw_04.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {
    private Order testorder;
    private  ArrayList<Item> items;
    private ShoppingCart cart;
    ShoppingCart cartNull;
    @BeforeEach
    void setUp() {
        Item item = new StandardItem(0, "smith", 500, "X", 5);
        Item item2 = new StandardItem(0, "smith", 500, "X", 4);
        Item item3 = new StandardItem(1, "johnson", 100, "Y", 3);
        items = new ArrayList<>();

        items.add(0, item);
        items.add(1, item2);
        items.add(2, item3);
        cart = new ShoppingCart(items);
        cartNull = new ShoppingCart(null);
        testorder = new Order(cart, "Jack", "USA", 0);
    }

    @Test
    void testOrderwithState(){
        Order order = new Order(cart, "Jack", "USA", 0);
        assertEquals(testorder.getItems(), order.getItems());
        assertEquals(testorder.getCustomerName(), order.getCustomerName());
        assertEquals(testorder.getCustomerAddress(), order.getCustomerAddress());
        assertEquals(testorder.getState(), order.getState());

    }
    @Test
    void testOrderwithoutState(){
        Order order = new Order(cart, "Jack", "USA");
        assertEquals(testorder.getItems(), order.getItems());
        assertEquals(testorder.getCustomerName(), order.getCustomerName());
        assertEquals(testorder.getCustomerAddress(), order.getCustomerAddress());
        assertEquals(testorder.getState(), order.getState());

    }
    @Test
    void testOrderNull(){
        Order order = new Order(cartNull, null, null);
        ShoppingCart cartNull1 = new ShoppingCart(null);
        assertEquals(cartNull1.getCartItems(), order.getItems());
        assertEquals(null, order.getCustomerName());
        assertEquals(null, order.getCustomerAddress());
        assertEquals(testorder.getState(), order.getState());

    }
}