package cz.cvut.fel.hw_04.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


class StandardItemTest {
    private StandardItem testitem;

    @BeforeEach
    void setUp() {
        testitem = new StandardItem(0, "smith", 500, "X", 5);
    }
    @Test
    void testConstructor(){
        StandardItem item = new StandardItem(0, "smith", 500, "X", 5);
        assertEquals(item, testitem);
    }
    @Test
    void copy() {
        StandardItem item = new StandardItem(0, "smith", 500, "X", 5);
        StandardItem copy = item.copy();
        assertEquals(copy, testitem);
    }


    @ParameterizedTest
    @MethodSource("testParameters")
    void testEquals(final Object items, boolean expected) {
        StandardItem item = new StandardItem(0, "smith", 500, "X", 5);

        assertEquals(equals(items), equals(item));

    }
     static Stream<Arguments> testParameters(){
        final StandardItem item1 = new StandardItem(0, "smith", 500, "X", 5);
        final StandardItem item2 = new StandardItem(0, "smith", 500, "X", 4);
        final StandardItem item3 = new StandardItem(1, "johnson", 100, "Y", 3);
        return Stream.of(
                Arguments.of(item1,  true),
                Arguments.of(item2, false),
                Arguments.of(item3, false)
        );
    }


}