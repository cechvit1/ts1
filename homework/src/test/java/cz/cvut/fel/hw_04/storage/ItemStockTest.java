package cz.cvut.fel.hw_04.storage;

import cz.cvut.fel.hw_04.shop.Item;
import cz.cvut.fel.hw_04.shop.StandardItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;
import cz.cvut.fel.hw_04.storage.ItemStock;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {
    private Item testitem;
    private ItemStock testitemstock;

    @BeforeEach
    void setUp() {
        testitem = new StandardItem(0, "smith", 500, "X", 5);
        testitemstock = new ItemStock(testitem);
    }
    @Test
    void testConstructor(){
        Item item = new StandardItem(0, "smith", 500, "X", 5);
        ItemStock itemStock = new ItemStock(item);
        assertEquals(testitemstock.getItem(), itemStock.getItem());
        assertEquals(testitemstock.getCount(), itemStock.getCount());

    }

    @ParameterizedTest
    @MethodSource("testParameter")
    void testIncreaseItemCount(StandardItem item, int x) {
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(x);
        assertEquals(itemStock.getCount(), x);
    }

    @ParameterizedTest
    @MethodSource("testParameter")
    void testDecreaseItemCount(StandardItem item, int y) {
        ItemStock itemStock = new ItemStock(item);
        itemStock.decreaseItemCount(y);
        assertEquals(itemStock.getCount(), -y);
    }


    static Stream<Arguments> testParameter(){
        final StandardItem item1 = new StandardItem(0, "smith", 500, "X", 5);
        final StandardItem item3 = new StandardItem(1, "johnson", 100, "Y", 3);
        final StandardItem item2 = new StandardItem(2, "jackson", 200, "Z", 10);
        return Stream.of(
                Arguments.of(item1,  1),
                Arguments.of(item2, 2),
                Arguments.of(item3, 7)
        );
    }
}