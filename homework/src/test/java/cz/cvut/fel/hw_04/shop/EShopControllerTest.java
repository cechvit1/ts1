package cz.cvut.fel.hw_04.shop;


import cz.cvut.fel.hw_04.archive.PurchasesArchive;
import cz.cvut.fel.hw_04.storage.NoItemInStorage;
import cz.cvut.fel.hw_04.storage.Storage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

class EShopControllerTest {
    ArrayList<ShoppingCart> carts;
    Storage storage;
    PurchasesArchive archive;
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errorStream = new ByteArrayOutputStream();


    @BeforeEach
    void setup() {
        System.setOut(new PrintStream(outputStream));
        System.setErr(new PrintStream(errorStream));
    }
    @AfterEach
    void resStreams(){
        System.setOut(originalOut);
        System.setErr(originalErr);

    }

    @Test
    void purchaseShoppingCart() {
        EShopController.startEShop();
        Assertions.assertNotNull(EShopController.getArchive());
        Assertions.assertNotNull(EShopController.getStorage());
        outputStream.reset();

        storage = EShopController.getStorage();
        carts = EShopController.getCarts();
        archive = EShopController.getArchive();


        storage.printListOfStoredItems();
        Assertions.assertEquals("STORAGE IS CURRENTLY CONTAINING:", outputStream.toString().trim());
        outputStream.reset();

        archive.printItemPurchaseStatistics();
        Assertions.assertEquals("ITEM PURCHASE STATISTICS:", outputStream.toString().trim());

        Assertions.assertEquals(0, carts.size());
        ShoppingCart shoppingCart = EShopController.newCart();
        Assertions.assertEquals(1, carts.size());



        outputStream.reset();


        try{
           EShopController.purchaseShoppingCart(shoppingCart, "Vít", "Troppau");
           Assertions.assertEquals("Error: shopping cart is empty\n", outputStream.toString());
        } catch (NoItemInStorage e){
            throw new RuntimeException(e);
        }

        Item[] items = {
                new StandardItem(1, "johnson", 100, "Y", 3),
                new StandardItem(0, "smith", 500, "X", 4),
                new DiscountedItem(2, "Chocolate Bar", 10, "Z", 10,  "1.1.2023", "1.1.2024")};


        int[] itemsCount = new int[]{4, 2, 3};

        for (int i = 0; i < items.length; i++){
            storage.insertItems(items[i], itemsCount[i]);
        }
        outputStream.reset();
        storage.printListOfStoredItems();
        Assertions.assertEquals("STORAGE IS CURRENTLY CONTAINING:\n" +
        "STOCK OF ITEM:  Item   ID 0   NAME smith   CATEGORY X   PRICE 500.0   LOYALTY POINTS 4    PIECES IN STORAGE: 2\n" +
        "STOCK OF ITEM:  Item   ID 1   NAME johnson   CATEGORY Y   PRICE 100.0   LOYALTY POINTS 3    PIECES IN STORAGE: 4\n" +
        "STOCK OF ITEM:  Item   ID 2   NAME Chocolate Bar   CATEGORY Z   ORIGINAL PRICE 10.0    DISCOUNTED PRICE 900.0  DISCOUNT FROM Sun Jan 01 00:00:00 CET 2023    DISCOUNT TO Mon Jan 01 00:00:00 CET 2024    PIECES IN STORAGE: 3", outputStream.toString().trim());


        ShoppingCart cart = new ShoppingCart();
        cart.addItem(items[0]);
        cart.addItem(items[1]);
        cart.addItem(items[2]);


        try {
            EShopController.purchaseShoppingCart(cart, "Adam", "Berlin");
        } catch (NoItemInStorage e){
            throw new RuntimeException(e);
        }

        outputStream.reset();
        storage.printListOfStoredItems();
        Assertions.assertEquals("STORAGE IS CURRENTLY CONTAINING:\n" +
                "STOCK OF ITEM:  Item   ID 0   NAME smith   CATEGORY X   PRICE 500.0   LOYALTY POINTS 4    PIECES IN STORAGE: 1\n" +
                "STOCK OF ITEM:  Item   ID 1   NAME johnson   CATEGORY Y   PRICE 100.0   LOYALTY POINTS 3    PIECES IN STORAGE: 3\n" +
                "STOCK OF ITEM:  Item   ID 2   NAME Chocolate Bar   CATEGORY Z   ORIGINAL PRICE 10.0    DISCOUNTED PRICE 900.0  DISCOUNT FROM Sun Jan 01 00:00:00 CET 2023    DISCOUNT TO Mon Jan 01 00:00:00 CET 2024    PIECES IN STORAGE: 2", outputStream.toString().trim());


        outputStream.reset();
        archive.printItemPurchaseStatistics();
        Assertions.assertEquals("ITEM PURCHASE STATISTICS:\n" +
                "ITEM  Item   ID 0   NAME smith   CATEGORY X   PRICE 500.0   LOYALTY POINTS 4   HAS BEEN SOLD 1 TIMES\n" +
        "ITEM  Item   ID 1   NAME johnson   CATEGORY Y   PRICE 100.0   LOYALTY POINTS 3   HAS BEEN SOLD 1 TIMES\n" +
        "ITEM  Item   ID 2   NAME Chocolate Bar   CATEGORY Z   ORIGINAL PRICE 10.0    DISCOUNTED PRICE 900.0  DISCOUNT FROM Sun Jan 01 00:00:00 CET 2023    DISCOUNT TO Mon Jan 01 00:00:00 CET 2024   HAS BEEN SOLD 1 TIMES", outputStream.toString().trim());

    }


}