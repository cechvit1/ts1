package cz.cvut.fel.ts1_selenium;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class test {
    WebDriver webDriver;
    SpringlerLink springlerLink;

    @BeforeEach
    void setUp() {
        webDriver = new FirefoxDriver();
        springlerLink = new SpringlerLink(webDriver);
    }

    @Test
    public void test_login_register(){
        springlerLink.visitPage(); //navštíví stránku

        springlerLink.setRegister(); //klikne na login/registr buttomn

        String expectedURL = "https://link.springer.com/signup-login?previousUrl=https%3A%2F%2Flink.springer.com%2F";
        String actualURL = webDriver.getCurrentUrl();

        Assertions.assertEquals(expectedURL, actualURL);

    }
    @Test
    public void test_search(){
        springlerLink.visitPage(); //navštíví stránku

        springlerLink.setSearch("Selenium"); //napíše selenium
        springlerLink.setClickSearch(); // klikne na search

        String expectedURL = "https://link.springer.com/search?query=Selenium";
        String actualURL = webDriver.getCurrentUrl();

        Assertions.assertEquals(expectedURL, actualURL);

    }
}
