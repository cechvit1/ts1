package cz.cvut.fel.ts1_selenium;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class test1 {
    WebDriver webDriver;
    SpringlerLinkLogin springlerLink;

    @BeforeEach
    void setUp() {
        webDriver = new FirefoxDriver();
        springlerLink = new SpringlerLinkLogin(webDriver);
    }

    @Test
    public void test_login(){
        springlerLink.visitPage();

        springlerLink.setEmail("cechvit1@fel.cvut.cz");
        springlerLink.setPassword("Hovno1");
        springlerLink.setCookies();
        springlerLink.setSubmit();

        String expected = "hov no";
        String actual = String.valueOf(webDriver.findElement(By.xpath("/html/body/div[4]/div[2]/div[1]/div[1]/button/span")).getText());


        Assertions.assertEquals(expected, actual);
    }

}
