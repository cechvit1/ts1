package cz.cvut.fel.ts1_selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SpringlerLinkLogin {
    WebDriver webDriver;

    @FindBy(id = "login-box-email")
    WebElement email;

    @FindBy(id = "login-box-pw")
    WebElement password;

    @FindBy(xpath = "/html/body/div[4]/div/form[1]/div/div[3]/button")
    WebElement submit;

    @FindBy(xpath = "/html/body/section/div/div[2]/button[1]")
    WebElement cookies;

    public SpringlerLinkLogin(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    public void visitPage(){webDriver.get("https://link.springer.com/signup-login?previousUrl=https://link.springer.com");}

    public void setEmail(String email) {
        this.email.sendKeys(email);
    }

    public void setPassword(String password) {
        this.password.sendKeys(password);
    }

    public void setSubmit() {
        this.submit.click();
    }

    public void setCookies() {
        this.cookies.click();
    }
}
