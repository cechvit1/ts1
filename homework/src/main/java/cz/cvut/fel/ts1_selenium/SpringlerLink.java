package cz.cvut.fel.ts1_selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SpringlerLink {

    WebDriver webDriver;

    @FindBy(className = "register-link")
    WebElement register;

    @FindBy(id = "query")
    WebElement search;
    @FindBy(id = "search")
    WebElement clickSearch;

    public SpringlerLink(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);

    }

    public void visitPage(){
        webDriver.get("https://link.springer.com/");
    }


    public void setRegister() {
        this.register.click();
    }

    public void setSearch(String search) {
        this.search.sendKeys(search);
    }

    public void setClickSearch() {
        this.clickSearch.click();
    }
}
